FROM rootproject/root-conda

RUN pip install uproot pyhf matplotlib

COPY . /code

WORKDIR /code


#FROM atlas/analysisbase:21.2.85-centos7
#
#RUN source ~/release_setup.sh
#RUN sudo yum install -y rh-python36 
#RUN sudo scl enable rh-python36 bash && sudo yum install -y pip && pip install uproot pyhf matplotlib
##RUN sudo scl enable rh-python36 bash && sudo yum install -y pip && pip install --upgrade pip && pip install uproot pyhf matplotlib
#
##COPY . /code
