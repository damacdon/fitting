# Fitting
This is the repo holding the code that creates the fitting docker image for the RECAST
tutorial.  If you found your way here as a participant, then this is the final code,
there are no other modifications that need to be made and you shouldn't even need this
code, only the image and a description of how to use it.

## Usage
The executable can be executed as
```
python run_fit.py --filesig selected_scaled.root --histsig h_mjj   --filebkg external_data.root --histbkg background  --filedata external_data.root --histdat data --outputfile limits.png --plotfile results.png
```
with the input arguments being :
  - `--filesig`    : The path to the input file containing your signal histogram 
  - `--histsig`    : The name of the signal histogram in that file
  - `--filebkg`    : The path to the input file containing your background histogram   
  - `--histbkg`    : The name of the background histogram in that file  
  - `--filedata`   : The path to the input file containing your data histogram    
  - `--histdat`    : The name of the data histogram in that file  
  - `--outputfile` : The `png` (or whatever) file name for saving your limits plot    
  - `--plotfile`   : The `png` (or whatever) file name for saving your data/MC plot 


## Results
This should produce two plots, a comparison of data and background (with signal too)
as well as the CLs scan.

![image](results.png)

![image](limits.png)